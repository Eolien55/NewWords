// How many words to generate
pub const K: usize = 100;

// Length boundaries
pub const MIN: usize = 2;
pub const MAX: usize = 100;

// N in N-grams
pub const N: usize = 5;

// Set demo mode
pub const DEMO: bool = false;

// Set verbose mode
pub const VERBOSE: bool = false;

// Set weighting
pub const WEIGHT: bool = false;
