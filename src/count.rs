use std::collections::{HashMap, HashSet};
use std::fs;
use std::io::{BufRead, BufReader};

use crate::config::{N, WEIGHT};

// Count all N-grams, return a hashmap containing
// the amount of each of them
pub fn count_from_file(file: &str) -> (HashMap<[char; N], usize>, HashSet<char>) {
    let mut result: HashMap<[char; N], usize> = HashMap::new();
    let mut found_chars: HashSet<char> = HashSet::new();

    // Opening the file, creating the buffer
    let fsfile = fs::File::open(file).expect("Failed to read file");
    let reader = BufReader::new(fsfile);

    let mut chars: [char; N];

    for (idx, line) in reader.lines().enumerate() {
        let idx = idx + 1;
        // We transform a Result<_> into a normal value
        let line = line.expect("Failed to read line") + "\n";
        let working_line: String;
        let weight: usize;

        if WEIGHT {
            let vec: Vec<_> = line.split("\t").collect();

            if vec.len() != 2 {
                panic!("File is in wrong format! It is supposed to be in TSV (with first field being the word and second its weight), but there are not _exactly_ 2 fields in the file you fed me (line {idx:#?} of file {file:#?})");
            }
            
            let num = vec.get(1).unwrap();
            let num = &num[0..num.len() - 2];

            working_line = vec.get(0).unwrap().to_string() + "\n";
            weight = num.parse().expect(&format!(
                "Second field is supposed to be a number (line {idx:#?} of file {file:#?})"
            ));
        } else {
            working_line = line;
            weight = 1;
        }

        // Initializing with null bytes
        chars = ['\0'; N];

        for last_char in working_line.chars() {
            found_chars.insert(last_char);

            // Updating the N-gram
            chars[N - 1] = last_char;
            result.insert(chars, result.get(&chars).unwrap_or(&0) + weight);
            // Preparing for the next iteration
            chars.rotate_left(1);
        }
    }

    (result, found_chars)
}
