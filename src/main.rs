use std::collections::{HashMap, HashSet};

// Sadly, we MUST use some 3d party library for random numbers
use rand::prelude::*;

pub mod config;
use config::*;
mod count;

use std::io;
use std::io::prelude::*;

fn main() {
    let args: Vec<_> = std::env::args().collect();

    if args.len() == 1 {
        println!("BUT NOOOOO YOU CAN'T JUST GIVE ME NO FILE AND EXPECT ME TO GUESS WHAT YOU'RE TRYING TO DO\n\nThe correct usage is {} <dic>...", args[0]);
    }

    let args: Vec<String> = args.iter().skip(1).cloned().collect();

    let callback = match DEMO {
        false => normal,
        true => demo,
    };

    for (idx, file) in args.iter().enumerate() {
        callback(file);
        if VERBOSE {
            println!(
                "\n\nDone file {}!{}",
                file,
                if idx == args.len() - 1 {
                    "\n\n".to_string()
                } else {
                    format!(" Now doing {}\n", args[idx + 1])
                }
            );
        }
    }
}

fn pause() {
    let mut stdin = io::stdin();

    // Read a single byte and discard
    let _ = stdin.read(&mut [0u8]).unwrap();
}

// Note : chars has N elements when only (N-1) are really used. This is
// for compliance with to_vector
fn demo(file: &str) {
    let (h, all_chars) = count::count_from_file(file);
    let mut rng: ThreadRng = thread_rng();

    let mut result = String::new();

    let mut chars: [char; N] = ['\0'; N];
    loop {
        println!(
            "Number of characters that can follow {:?} :",
            chars
                .iter()
                .filter(|&&c| c != '\0')
                .take(N - 1)
                .collect::<String>()
        );

        let mut possible_chars = to_vector(chars, &h, &all_chars);
        let sum = possible_chars.iter().map(|&(_, we)| we).sum();

        possible_chars.sort_by(|&(_, a), &(_, b)| b.cmp(&a));
        for (da_char, weight) in possible_chars.iter() {
            println!(
                "\t{:?} with {}% probability",
                da_char,
                (*weight as f64 / sum as f64) * 100.0
            );
        }

        let (grown_vector, _) = grow_vector(&possible_chars);
        let new_char = random_char(&grown_vector, sum, &mut rng);

        println!(
            "Picked {:?} with {}% probability",
            new_char,
            (possible_chars[possible_chars
                .iter()
                .position(|&(c, _)| c == new_char)
                .unwrap()]
            .1 as f64
                / sum as f64)
                * 100.0
        );
        if new_char == '\n' {
            println!("{:?} means the end of the word.\n", new_char);
            break;
        }

        result.push(new_char);
        chars.rotate_left(1);
        chars[N - 2] = new_char;

        println!("Word so far : {}", result);
        pause();
    }
    println!("Word : {}", result);
}

// Note : chars has N elements when only (N-1) are really used. This is
// for compliance with to_vector
fn normal(file: &str) {
    let (h, all_chars) = count::count_from_file(file);

    let mut words: Vec<String> = Vec::with_capacity(K);

    let mut chars: [char; N];
    let mut word: String;
    let mut rng: ThreadRng = thread_rng();
    while words.len() < K {
        chars = ['\0'; N];
        word = String::new();

        loop {
            let (growed_vector, sum) = grow_vector(&to_vector(chars, &h, &all_chars));
            let new_char = random_char(&growed_vector, sum, &mut rng);

            if new_char == '\n' {
                break;
            }

            word.push(new_char);
            chars.rotate_left(1);
            chars[N - 2] = new_char;
        }

        if MIN < word.len() && word.len() < MAX {
            words.push(word);
        }
    }

    for word in words {
        println!("{}", word);
    }
}

// This finds the first n where :
// data[n] >= random > data[n-1]
// If n doesn't exist (ie, if no N-gram start by this sequence),
// a \n character is returned
fn random_char(data: &Vec<(char, usize)>, sum: usize, rng: &mut ThreadRng) -> char {
    if sum == 0 {
        return '\n';
    }

    let random = rng.gen_range(0..sum);
    let (mut left, mut right): (usize, usize) = (0, data.len());
    let mut p: usize;
    loop {
        p = (left + right) / 2;
        if data[p].1 > random {
            if p == 0 || data[p - 1].1 <= random {
                return data[p].0;
            } else {
                right = p;
            }
        } else {
            left = p;
        }
    }
}

// Get a vector of (char, usize) for all N-grams that begin by base_chars where the char represents
// the last char and usize how many times it appeared in the sequence

// base_chars is a (N-1)-gram, but its size must be N, so this function is able to add a char at
// the end
fn to_vector(
    base_chars: [char; N],
    h: &HashMap<[char; N], usize>,
    all_chars: &HashSet<char>,
) -> Vec<(char, usize)> {
    let mut result: Vec<(char, usize)> = vec![];
    let mut vector = base_chars;
    for da_char in all_chars {
        vector[N - 1] = *da_char;
        match h.get(&vector) {
            Some(w) => result.push((*da_char, *w)),
            None => (),
        }
    }

    result
}

// Makes vector "grow", ie vec[i] = vec[i - 1] + oldvec[i]
fn grow_vector(vec: &[(char, usize)]) -> (Vec<(char, usize)>, usize) {
    let mut ret_vec: Vec<(char, usize)> = vec![];
    for (i, &(ch, we)) in vec.iter().enumerate() {
        ret_vec.push((ch, we + ret_vec.get(i - 1).map(|&(_, w)| w).unwrap_or(0)));
    }
    (ret_vec, vec.iter().map(|&(_, we)| we).sum())
}
