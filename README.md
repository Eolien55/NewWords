# New-word Generator

This simple program generates random words, that could be real words,
written in Rust.

I used really simple algorithms to achieve this, so the result isn't totally
perfect.

## Motivation

I was **HEAVILY** inspired by [ScienceEtonnante's own algorithm, and
video](https://www.youtube.com/watch?v=YsR7r2378j0) (link for french folks).
Unfortunately, the code was pretty ugly (unnamed variables, like i, j, etc.,
hard-coded values, inconstitencies in code...), and really slow. The code used
to parse the dictionary was indeed written in Python (though the code used to
generate the words heavily relied on NumPy, which is known to be extremely
fast). Some parts of the code were really slow due to being basically trial and
error (O(∞) if you're unlucky), and since the algorithm was fairly simple, I
decided to rewrite it.

We lost some features though. ScienceEtonnante's algorithm could draw graphs to
represent the dictionary, while Rust AFAIK doesn't have such things as
matplotlib.pyplot (if you guys know a tool to serve this very purpose, don't
hesitate to tell me, it would be very sweet of you). I think I can write a
simple GnuPlot script to do this, but I feel like this would be extremely
hacky.

## How it works

The algorithm has the 2 last characters stored in memory, and it tries to
predict the 3d. Then, it uses the 2nd as the 1st, the 3d as the 2nd and tries
to predict the new 3d character. This is basic Markov chains.

So, the word is generated until the last character represents the end of a
word, aka a newline character.

Then, we output the new generated word (with a few rules, like the number of
words of a certain length, or the maximum and minimum sizes. These are
configured by editing the source code ([see below](#options))). And we repeat the
process, until we generated the right amount of words, of the right sizes.

## Warning

In english, the result is very poor. I suppose that it is because english words
are very different, and because the spelling "rules" aren't as strict as in
other languages (like french, or swedish).

The french result, however, is fairly good in my opinion. Swedish and spanish
look like words that could actually exist.

## How to use

To use you must have : a copy of this program, and a dictionary ([see below if you don't have one](#libreoffice-dictionaries)).

To get this program, do :

`git clone https://gitlab.com/Eolien55/NewWordGenerator`

`cd NewWordGenerator`

You should also have Cargo installed.

Then, run `cargo run --release -- <dictionary.dic>...` to get a list of new
words ! 

### LibreOffice dictionaries

To obtain a dictionary, you can do the following :

Search for a dictionary at
<https://cgit.freedesktop.org/libreoffice/dictionaries/tree>. The dictionary
you get might not be perfect, so you can change it to your needs. What I
recommend : the words use this scheme `<file>/<type>`. So, you can run
something like `sed -i 's#^.*/<type>$##' <dictionary.dic>`, followed by a `sed
-i '/^$/d' <dictionary.dic>` to remove empty lines.

What I personaly use :

```bash
for pat in <pattern-list>; do
	sed -i "s#^.*/$pat\$##" <dictionary.dic>
done
 
sed -i '/^$/d' <dictionary.dic>
  
sed -i 's#/.*$##' <dictionary.dic>
```

Just replace `<pattern-list>` by a list of patterns seperated by commas and
enclosed by quotes, and `<dictionary.dic>` by the actual file you are treating.

What this does :
* get a list of type of words to remove
* delete all the lines that are of this type
* repeat until the list is empty
* delete all empty lines
* and, finally, delete the second part of the word, aka the `/<type>`.

## Options

Many options are configurable via `config.rs` : the "n" in "n-grams" (this can be
seen as a precision value ; the more it is high the more it will look like the target
language ; but the more it is high, the less likely it is to create new words),
the number of words to generate (`K` for some reason), the minimum and maximum
word lengths (`MIN` and `MAX` with values 2 and 100 by default, respectively ; these default
values are meant to be changed for experimentation), demo mode ([see below](#demo)), verbose mode
(more output), and WEIGHT option ([see below](#ponderate)).

### Demo

You can also make a demo of the program by running `cargo run --release --
<dictionary.dic>` with `DEMO` set to `true` in `config.rs`. In demo mode, only
one word will be generated, of one dictionary only.

### Ponderate

Some results in some languages are _awful_. For example, English looks nothing
like English. This is because of the fact that for this program, each word is just
as frequent (which is false). To fix this problem, one may use "ponderated dictionaries",
such as the ones provided by [Google's Ngram](https://storage.googleapis.com/books/ngrams/books/datasetsv3.html).

Ponderated dictionaries are of the following format : `example<tab>587`. The
program will panic if the format is not strictly followed (remember, one word,
one tab, one number per line).

## Licensing

This is free software, and licensed under the GPLv3 license.

## Contribute

If you have any idea to improve the algorithm, please let me know.
